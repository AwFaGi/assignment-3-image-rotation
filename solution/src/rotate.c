#include "../include/rotate.h"
#include "stdio.h"
struct image rotate(const struct image* src){

    struct image new_image = create_image(src->height, src->width);

    for (size_t height_count = 0; height_count < src->height; height_count++){
        for (size_t width_count = 0; width_count < src->width; width_count++){
            set_pixel(&new_image, (src->height - 1) - height_count, width_count,
                      get_pixel(src, width_count, height_count)
            );
        }
    }

    return new_image;
}
