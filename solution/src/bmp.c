#include "../include/bmp.h"
#include "malloc.h"

size_t get_padding(const size_t width){

    if(width % 4 == 0){
        return 0;
    }
    return 4 - ((width * 3) % 4);
}

struct bmp_header fill_header(const struct image* image){

    size_t size = (image->width + get_padding(image->width)) * image->height * sizeof(struct pixel);

    struct bmp_header bmp_header = {
            .bfType = 19778,
            .bfileSize = size + sizeof(struct bmp_header),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };

    return bmp_header;
}

enum read_status from_bmp( FILE* in, struct image* img ){
    struct bmp_header header = {0};

    size_t count = fread(&header, sizeof(struct bmp_header), 1, in);
    if (count != 1){
        return READ_INVALID_HEADER;
    }

    *img = create_image(header.biWidth, header.biHeight);

    size_t padding = get_padding(header.biWidth);

    for (size_t i = 0; i < img->height; ++i){
        if (fread(img->data + (i * img->width), sizeof(struct pixel), img->width, in) != img->width) {
            free_image(img);
            return READ_INVALID_BITS;
        }

        if (fseek(in, (long) padding, SEEK_CUR) != 0) {
            free_image(img);
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ){
    struct bmp_header header = fill_header(img);

    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)){
        return WRITE_ERROR;
    }

    size_t padding = get_padding(img->width);
    uint8_t padding_filler = 0;

    for (size_t i = 0; i < img->height; i++){
        if (fwrite(img->data + (i * img->width), sizeof(struct pixel), img->width, out) != img->width){
            return WRITE_ERROR;
        }

        for (size_t j = 0; j < padding; j++){
            if (!fwrite(&padding_filler, sizeof(uint8_t), 1, out)){
                return WRITE_ERROR;
            }
        }
    }

    return WRITE_OK;
}
