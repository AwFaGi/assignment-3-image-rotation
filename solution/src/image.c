#include "../include/image.h"
#include "malloc.h"
#include "stdint.h"

struct image create_image(const uint64_t width, const uint64_t height){
    struct image image = {0};

    image.width = width;
    image.height = height;

    struct pixel* data = malloc(width * height * sizeof(struct pixel));

    image.data = data;
    return image;
}

void free_image(struct image* image){
    free(image->data);
}

struct pixel get_pixel(const struct image* image, const uint64_t x, const uint64_t y){
    return image->data[y * image->width + x];
}

void set_pixel(const struct image* image, const uint64_t x, const uint64_t y, const struct pixel new_pixel){
    if (image->data == NULL){
        return;
    }
    image->data[y * image->width + x] = new_pixel;
}
