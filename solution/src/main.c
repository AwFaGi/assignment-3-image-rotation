#include "image.h"
#include "bmp.h"
#include "errors.h"
#include "rotate.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    if (argc != 3){
        fprintf(stderr, "Use me like this:\n./image-transformer <source-image> <transformed-image>\n");
        return INVALID_ARGUMENTS_ERROR;
    }

    FILE* in = fopen(argv[1], "rb");
    if (in == NULL){
        fprintf(stderr, "Error on opening <source-image> file\n");
        return OPENING_SOURCE_IMAGE_ERROR;
    }

    struct image img = {0};

    enum read_status status = from_bmp(in, &img);
    if (status != READ_OK){
        fprintf(stderr, "Unable to read <source-image> file because of invalid ");
        switch (status) {

            case READ_INVALID_BITS:
                fprintf(stderr, "bits\n");
                break;
            case READ_INVALID_HEADER:
                fprintf(stderr, "header\n");
                break;
            default:
                fprintf(stderr, "something\n");
        }
        free_image(&img);
        return status;
    }

    if (fclose(in) != 0){
        fprintf(stderr, "Unable to close <source-image> file\n");
        free_image(&img);
        return CLOSING_SOURCE_IMAGE_ERROR;
    };

    struct image output_image = rotate(&img);
    free_image(&img);

    FILE* out = fopen(argv[2], "wb");
    if (out == NULL){
        fprintf(stderr, "Error on opening <transformed-image> file\n");
        free_image(&output_image);
        return OPENING_TRANSFORMED_IMAGE_ERROR;
    }

    enum write_status write_status = to_bmp(out, &output_image);
    if (write_status != WRITE_OK){
        fprintf(stderr, "Unable to write <transformed-image> file\n");
        free_image(&output_image);
        return WRITING_TRANSFORMED_IMAGE_ERROR;
    }

    if (fclose(out) != 0){
        fprintf(stderr, "Unable to close <transformed-image> file\n");
        free_image(&output_image);
        return CLOSING_TRANSFORMED_IMAGE_ERROR;
    };

    free_image(&output_image);

    printf("Working!\n");
    return 0;
}
