#ifndef ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
#include  <stdint.h>

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image create_image(const uint64_t width, const uint64_t height);
void free_image(struct image* image);

struct pixel get_pixel(const struct image* image, const uint64_t x, const uint64_t y);
void set_pixel(const struct image* image, const uint64_t x, const uint64_t y, const struct pixel new_pixel);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
